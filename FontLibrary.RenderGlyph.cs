﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace IndigoFont
{
    public partial class FontLibrary
    {
        private class RenderGlyph
        {
            public RenderGlyph(Texture2D texture, Rectangle bounds, Vector2 position)
            {
                Texture = texture;
                Bounds = bounds;
                Position = position;
            }

            public Texture2D Texture { get; }
            public Rectangle Bounds { get; }
            public Vector2 Position { get; }
        }
    }
}