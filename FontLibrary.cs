﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using SharpFont;

namespace IndigoFont
{
    public sealed partial class FontLibrary : IDisposable
    {
        private static int TextureSize = 1024;

        public FontLibrary(Stream fontStream, GraphicsDevice graphicsDevice)
        {
            GraphicsDevice = graphicsDevice;

            using (var ms = new MemoryStream())
            {
                fontStream.CopyTo(ms);
                fontBytes = ms.ToArray();
            }
        }

        public IFont CreateFont(int size)
        {
            if (!fonts.TryGetValue(size, out var font))
                font = new Font(size, fontBytes, GraphicsDevice);

            return font;
        }

        public void Dispose()
        {
            foreach (var font in fonts.Values)
                font.Dispose();

            _lib.Dispose();
        }

        private readonly Dictionary<int, IFont> fonts = new Dictionary<int, IFont>();
        private static Library _lib = new Library();
        private readonly GraphicsDevice GraphicsDevice;
        private readonly byte[] fontBytes;
    }
}
