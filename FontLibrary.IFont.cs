﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace IndigoFont
{
    public partial class FontLibrary
    {
        public interface IFont : IDisposable
        {
            char? DefaultCharacter { get; set; }
            int LineHeight { get; }
            int Size { get; }
            int TabSpaces { get; set; }

            IText MakeText(string text);
            void PreheatCache(IEnumerable<char> chars);
        }

        public interface IText
        {
            float Width { get; }
            float Height { get; }
            string String { get; }
            
            void Draw(SpriteBatch spriteBatch, Vector2 position, Color color);
        }
    }
}