# IndigoFont

Library by jacobalbano to allow SharpFont text rendering in FNA

**DISCLAIMER: None of this code is written by me!** It is created by [jacobalbano](https://bitbucket.org/jacobalbano) as part of [Indigo3](https://bitbucket.org/jacobalbano/indigo3). I have simply isolated the font rendering code to be used as part of [Net.Sockets](https://gitlab.com/aecsocket/net-sockets).

## Libraries required

- [Client/lib/FNA.dll](https://github.com/FNA-XNA/FNA)
- [Client/lib/SharpFont.dll](https://github.com/Robmaister/SharpFont/)

## Installation

1. Compile the solution in your IDE of choice.
2. Add `obj/Debug/IndigoFont.dll` as a reference to your FNA project.
3. Follow the [Usage](#usage) section.

## Usage

To use a font, create a new `IndigoFont.FontLibrary` instance with a `FileStream` of your font and an XNA `GraphicsDevice`.  
Use `FontLibrary.CreateFont(size)` to create a `FontLibrary.IFont` which you can use to render text.  
To cache text, use `FontLibrary.IFont.MakeText(string)` to create a `FontLibrary.IText` which you can pass into an XNA `SpriteBatch.DrawString()`.  
To find text size, use `FontLibrary.IText.Width` and `FontLibrary.IText.Height`. No need for `MeasureString`!
