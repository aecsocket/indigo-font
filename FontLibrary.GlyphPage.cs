﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace IndigoFont
{
    public partial class FontLibrary
    {
        private class GlyphPage : IDisposable
        {
            private readonly int TextureSize;
            private readonly Color[] colors;

            public Texture2D Texture { get; }
            private BinPacker Packer { get; }

            public GlyphPage(GraphicsDevice graphicsDevice, int textureSize)
            {
                TextureSize = textureSize;
                Packer = new BinPacker(textureSize, textureSize);
                colors = new Color[textureSize * textureSize];
                for (int i = textureSize * textureSize; i-- > 0;)
                    colors[i] = new Color(255, 255, 255, 0);

                Texture = new Texture2D(graphicsDevice, textureSize, textureSize, false, SurfaceFormat.Color);
            }

            public bool Pack(int w, int h, out Rectangle rect)
            {
                return Packer.Pack(w, h, out rect);
            }

            public void RenderGlyph(int width, int height, byte[] bitmap, int x, int y)
            {
                for (int by = 0; by < height; by++)
                {
                    for (int bx = 0; bx < width; bx++)
                    {
                        var src = by * width + bx;
                        var dest = (by + y) * TextureSize + bx + x;
                        colors[dest] = new Color(255, 255, 255, bitmap[src]);
                    }
                }

                Texture.SetData(colors);
            }

            public void Dispose()
            {
                Texture.Dispose();
            }
        }
    }
}